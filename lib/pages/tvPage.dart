import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:http/http.dart' as http;

//models
import 'package:starcines/models/movie_detail.dart';
import 'package:starcines/models/tvshow_detail.dart';

final Color pcolor = Color(0xFFff1744);
final String themoviedb = "20ac754dbe6ba584a7a8a8690c755249";
final String themoviedbIMG = "https://image.tmdb.org/t/p/w500/";

class TvPage extends StatefulWidget {
  final int tvId;

  TvPage({Key key, @required this.tvId}) : super(key: key);

  @override
  _TVPageState createState() => new _TVPageState();
}

class _TVPageState extends State<TvPage> {
  var youtube = new FlutterYoutube();
  TVShowDetail tv;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  var trailer = ""; //trailer de la pelicula
  var runtimeH = ""; //duracion de la pelicula
  var runtimeM = ""; //duracion de la pelicula
  var genres = ""; //generos de la pelicula
  var tab = 0; //generos de la pelicula
  List<String> imgs = <String>[];

  BuildContext _context;

  @override
  void initState() {
    super.initState();
    fetchTV();
  }

  /**
   * recupera los datos de la pelicula sgun el id
   */
  fetchTV() async {
    var url = 'https://api.themoviedb.org/3/tv/${widget
        .tvId}?api_key=$themoviedb&language=es';
    print(url);
    final response = await http.get(url);
    var parsed = JSON.decode(response.body);

    if (parsed['id'] != null) {
      var tmp = TVShowDetail.fromJSON(parsed);
      print("tv ${tmp.name}");
      var index = 0;
      for (var genre in tmp.genres) {
        genres +=
            "${genre['name']}${index < tmp.genres.length - 1 ? ',' : ''} ";
        index++;
      }

      setState(() {
        tv = tmp;
      });
    }
  }

  List<Widget> _buildCategoryChips() {
    return ["Accion", "Comedia"].map((category) {
      return Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: Chip(
          label: Text(
            category,
            style: TextStyle(fontSize: 10.0),
          ),
          backgroundColor: Colors.black12,
        ),
      );
    }).toList();
  }

  Widget TVDetailHeader() {
    var screenWidth = MediaQuery.of(_context).size.width;
    return SliverToBoxAdapter(
      child: Stack(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.only(bottom: 30.0),
            child: ClipPath(
              clipper: ArcClipper(),
              child: Image.network(
                "https://image.tmdb.org/t/p/w500/${tv
                    .backdrop_path}",
                width: screenWidth,
                height: 290.0,
                fit: BoxFit.cover,
                colorBlendMode: BlendMode.srcOver,
                color: new Color.fromARGB(120, 20, 10, 40),
              ),
            ),
          ),
          new Positioned(
            left: 20.0,
            right: 5.0,
            bottom: 10.0,
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Material(
                  borderRadius: BorderRadius.circular(4.0),
                  elevation: 5.0,
                  child: new CachedNetworkImage(
                    imageUrl: "https://image.tmdb.org/t/p/w500/${tv
                        .poster_path}",
                    width: 150.0,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        "IMDb",
                        style: TextStyle(
                            fontSize: 22.0,
                            fontFamily: 'Anton',
                            color: Colors.white),
                      ),
                      new Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          SizedBox(width: 5.0),
                          Text(
                            tv.vote_average.toString(),
                            style:
                                TextStyle(fontSize: 24.0, color: Colors.white),
                          ),
                          Text(
                            "/10",
                            style: TextStyle(
                                fontSize: 24.0, color: Color(0xFFd5d9d9d9)),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          new Positioned(
              top: 25.0,
              left: 10.0,
              right: 10.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back, color: Colors.white),
                    onPressed: () {
                      Navigator.pop(_context);
                    },
                  ),
                  Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.bookmark_border, color: Colors.white),
                        onPressed: () {
                          Navigator.pop(_context);
                        },
                      ),
                      IconButton(
                        icon: Icon(Icons.favorite_border, color: Colors.white),
                        onPressed: () {
                          Navigator.pop(_context);
                        },
                      ),
                    ],
                  )
                ],
              )),
          new Positioned(
            bottom: -7.0,
            right: 0.0,
            child: GestureDetector(
              onTap: () {
                youtube.playYoutubeVideoById(
                    apiKey: "AIzaSyBCfP7rIsyrywYhfuRJ4KUIDweeH7mbkSI",
                    videoId: trailer,
                    fullScreen: true //default false
                    );
              },
              child: Stack(
                children: <Widget>[
                  ClipPath(
                    clipper: TriClipper(),
                    child: Container(
                      width: screenWidth,
                      height: 290 / 2 + 15,
                      color: Colors.amber,
                    ),
                  ),
                  Positioned(
                    top: 290 / 8,
                    right: screenWidth / 12,
                    child: Center(
                        child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 50.0,
                    )),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSipnosis() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
      child: Column(
        children: <Widget>[
          Text(
            "Sipnosis:",
            textAlign: TextAlign.start,
            style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 5.0),
          Text(
            tv.overview,
            textAlign: TextAlign.justify,
            style: TextStyle(color: Color(0xFF000000)),
          )
        ],
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }

  Widget _buildHead2() {
    return SliverToBoxAdapter(
      child: Padding(
        padding: EdgeInsets.only(left: 20.0, top: 15.0),
        child: new Padding(
          padding: EdgeInsets.only(right: 10.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                tv.name,
                style: TextStyle(
                    fontSize: 23.0,
                    fontWeight: FontWeight.w800,
                    color: Colors.white),
                maxLines: 1,
                softWrap: true,
              ),
              SizedBox(
                height: 5.0,
              ),
              Container(
                height: 32.0,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: tv.genres.map((mgenre) {
                    return Padding(
                      padding: EdgeInsets.only(right: 5.0),
                      child: Chip(
                        label: Text(
                          mgenre['name'],
                          style: TextStyle(color: Colors.white),
                        ),
                        backgroundColor: Colors.blueGrey,
                      ),
                    );
                  }).toList(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTabs() {
    return new SliverToBoxAdapter(
      child: Padding(
        padding: EdgeInsets.only(top: 35.0),
        child: Material(
          elevation: 5.0,
          color: Color(0xFFf5f5f5),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              new Expanded(
                child: FlatButton(
                  child: Text(
                    "INFO",
                    style: TextStyle(color: tab == 0 ? pcolor : Colors.black),
                  ),
                  onPressed: () {
                    setState(() {
                      tab = 0;
                    });
                  },
                  padding: EdgeInsets.all(15.0),
                ),
              ),
              new Expanded(
                child: FlatButton(
                  child: Text(
                    "TEMPORDAS (${tv.number_of_seasons})",
                    style: TextStyle(color: tab == 1 ? pcolor : Colors.black),
                  ),
                  onPressed: () {
                    setState(() {
                      tab = 1;
                    });
                  },
                  padding: EdgeInsets.all(15.0),
                ),
              ),
              new Expanded(
                child: FlatButton(
                  child: Text(
                    "REVIEWS",
                    style: TextStyle(color: tab == 2 ? pcolor : Colors.black),
                  ),
                  onPressed: () {
                    setState(() {
                      tab = 2;
                    });
                  },
                  padding: EdgeInsets.all(15.0),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _tab0() {
    return SliverToBoxAdapter(
      child: Material(
        color: Color(0xFFf5f5f5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Padding(
              padding: EdgeInsets.only(left: 20.0,top: 20.0),
              child: new Row(
                children: <Widget>[
                  Text(
                    "Cadena:",
                    style:
                        TextStyle(fontSize: 19.0, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(width: 5.0),
                  Expanded(
                    child: Container(
                      height: 40.0,

                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          var network = tv.networks[index];
                          return Image.network(
                            "$themoviedbIMG${network['logo_path']}",
                            height: 30.0,
                          );
                        },
                        itemCount: tv.networks.length,
                      ),
                    ),
                  )
                ],
              ),
            ),
            _buildSipnosis(),
            new Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
              child: new Column(
                children: <Widget>[
                  new Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Primera emisión:",
                              style: TextStyle(
                                  fontSize: 19.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 5.0),
                            Text(
                              "${tv.first_air_date}",
                              style: TextStyle(fontSize: 19.0),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Ultima emisión:",
                              style: TextStyle(
                                  fontSize: 19.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 5.0),
                            Text(
                              "${tv.last_air_date}",
                              style: TextStyle(fontSize: 19.0),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20.0),
                  new Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Total de episodios:",
                              style: TextStyle(
                                  fontSize: 19.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 5.0),
                            Text(
                              "${tv.number_of_episodes}",
                              style: TextStyle(fontSize: 19.0),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Text(
                              "En producción:",
                              style: TextStyle(
                                  fontSize: 19.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 5.0),
                            Text(
                              "${tv.in_production == true
                                  ? 'SI'
                                  : 'NO'}",
                              style: TextStyle(fontSize: 19.0),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 20.0),
                ],
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _tab1() {
    return SliverList(
      delegate: new SliverChildBuilderDelegate((context, index) {
        var img = imgs[index];
        return new CachedNetworkImage(
          imageUrl: "https://image.tmdb.org/t/p/w500/${img}",
          placeholder: Container(
            child: new Center(
              child: new CircularProgressIndicator(),
            ),
            width: 100.0,
            height: 100.0,
          ),
          errorWidget: Container(
            child: new Icon(Icons.error),
            width: 100.0,
            height: 100.0,
          ),
          fit: BoxFit.cover,
          height: 200.0,
          width: 100.0,
        );
      }, childCount: imgs.length),
    );
  }

  @override
  Widget build(BuildContext context) {
    this._context = context;
    return new Scaffold(
        key: _scaffoldKey,
        body: tv != null
            ? CustomScrollView(
                slivers: <Widget>[
                  TVDetailHeader(),
                  _buildHead2(),
                  _buildTabs(),
                  tab == 0 ? _tab0() : (tab == 1 ? _tab1() : _tab1())
                ],
              )
            : new Center(
                child: new CircularProgressIndicator(),
              ));
  }
}

class TriClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(size.width / 2, size.height / 3);
    path.lineTo(size.width, size.height / 1.3);
    path.lineTo(size.width, 0.0);
    path.lineTo(size.width / 2, size.height / 3);
    path.lineTo(size.width / 2, size.height / 3);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

class ArcClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(0.0, size.height - 140.0);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class Casting {
  final String name;
  final String profile_path;

  Casting({this.name, this.profile_path});

  factory Casting.fromJson(Map<String, dynamic> json) {
    return Casting(name: json['name'], profile_path: json['profile_path']);
  }
}
