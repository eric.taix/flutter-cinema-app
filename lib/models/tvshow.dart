class TVShow {
  final String poster_path;
  final double popularity;
  final int id;
  final String backdrop_path;
  final double vote_average;
  final String overview;
  final String first_air_date;
  final List<dynamic> origin_country;
  final List<dynamic> genre_ids;
  final String original_language;
  final int vote_count;
  final String name;
  final String original_name;

  TVShow(
      {this.poster_path,
      this.popularity,
      this.id,
      this.backdrop_path,
      this.vote_average,
      this.overview,
      this.first_air_date,
      this.origin_country,
      this.genre_ids,
      this.original_language,
      this.vote_count,
      this.name,
      this.original_name});


  factory TVShow.fromJSON(Map<String, dynamic> json){
    return TVShow(
      poster_path: json['poster_path'],
      popularity: json['popularity'],
      id:json['id'],
      backdrop_path: json['backdrop_path'],
      vote_average: json['vote_average'].toDouble(),
      overview: json['overview'],
      first_air_date: json['first_air_date'],
      origin_country: json['origin_country'],
      genre_ids: json['genre_ids'],
      original_language: json['original_language'],
      vote_count: json['vote_count'],
      name: json['name'],
      original_name: json['original_name']
    );
  }

}
