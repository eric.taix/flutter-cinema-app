import 'package:starcines/models/season.dart';

class TVShowDetail {
  final String poster_path;
  final double popularity;
  final int id;
  final String backdrop_path;
  final double vote_average;
  final String overview;
  final String first_air_date;
  final String last_air_date;
  final List<dynamic> origin_country;
  final List<dynamic> genres;
  final List<Season> seasons;
  final List<dynamic> networks;
  final String original_language;
  final int vote_count;
  final int number_of_seasons;
  final int number_of_episodes;
  final String name;
  final String original_name;
  final bool in_production;

  TVShowDetail(
      {this.poster_path,
      this.popularity,
      this.id,
      this.backdrop_path,
      this.vote_average,
      this.overview,
      this.first_air_date,
      this.last_air_date,
      this.origin_country,
      this.genres,
      this.seasons,
      this.networks,
      this.original_language,
      this.vote_count,
      this.number_of_episodes,
      this.number_of_seasons,
      this.name,
      this.original_name,
      this.in_production});

  factory TVShowDetail.fromJSON(Map<String, dynamic> json) {
    return TVShowDetail(
        poster_path: json['poster_path'],
        popularity: json['popularity'],
        id: json['id'],
        backdrop_path: json['backdrop_path'],
        vote_average: json['vote_average'].toDouble(),
        overview: json['overview'],
        first_air_date: json['first_air_date'],
        last_air_date: json['last_air_date'],
        origin_country: json['origin_country'],
        genres: json['genres'],
        networks: json['networks'],
        original_language: json['original_language'],
        vote_count: json['vote_count'],
        number_of_episodes: json['number_of_episodes'],
        number_of_seasons: json['number_of_seasons'],
        name: json['name'],
        original_name: json['original_name'],
        in_production: json['in_production'],
        seasons: Season.getSeasons(json['seasons']));
  }
}
